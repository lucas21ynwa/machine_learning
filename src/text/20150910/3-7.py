#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import random
from numpy import linalg as la


def gaussian(x, mu, sigma):
    gauss = np.exp(-0.5 * ((x - mu) / sigma) ** 2) / \
            (np.sqrt(2.0 * np.pi) * sigma)

    return(gauss)


def t(a0, a1, x):
    # t = a0 + a1 * xi + error
    mu = 0.0
    sigma = 0.2
    return a0 + a1 * x + random.gauss(mu, sigma)


def f(w0, w1, x):
    # f = w0 + w1 * xi
    return w0 + w1 * x


def calc_likelifood(beta, t, x, w):
    # 観測値1つの対数尤度を求める
    # logL(w) = -\frac{\beta}{2}(t-w^{T}\phi(x))^2 + cons
    # を利用する。対数尤度関数でプロットしたい範囲の$w$を求める。
    w = np.array(w)
    phi_x = np.array([1, x])
    return -1 * beta * 0.5 * (t - w.T.dot(phi_x)) ** 2


def draw_likelihood(beta, t, x, title='', ax=None):
    # 観測値の尤度のプロット
    w0 = np.linspace(-1.0, 1.0, 100)
    w1 = np.linspace(-1.0, 1.0, 100)
    W0, W1 = np.meshgrid(w0, w1)
    L = []
    for w0i in w0:
        L.append([calc_likelifood(beta, t, x, [w0i, w1i]) for w1i in w1])
        # ax.pcolor(W0, W1, np.array(L).T, cmap=plt.cm.jet, vmax=0, vmin=-1)
    
    ax.contourf(W0, W1, np.array(L).T, 512, vmax=0, vmin=-1)
    ax.set_xlabel('$w_0$')
    ax.set_ylabel('$w_1$')
    # ax.set_title(title)
    ax.set_aspect(1.0)
    return 0


def draw_probability(ax, M, S):
    sigma_w0 = np.sqrt(S[0, 0])
    sigma_w1 = np.sqrt(S[1, 1])
    sigma_w0w1 = S[0, 1]
    mu_w0 = M[0, 0]
    mu_w1 = M[1, 0]

    u = np.linspace(-1.0, 1.0, 100)
    v = np.linspace(-1.0, 1.0, 100)
    U, V = np.meshgrid(u, v)
    W = mlab.bivariate_normal(U, V,
                              sigma_w0, sigma_w1,
                              mu_w0, mu_w1,
                              sigma_w0w1)
    ax.contourf(U, V, W, 512)
    ax.set_xlabel('$w_0$')
    ax.set_ylabel('$w_1$')
    ax.set_aspect(1.0)

    return 0


if __name__ == '__main__':
    alpha = 2.0
    beta = 25

    a0 = -0.3
    a1 = 0.5

    M = np.array([[0.0], [0.0]])
    S = np.array([[1000.0, 0.0], [0.0, 1000.0]])
    I = np.matrix(np.identity(2))

    fig = plt.figure()

    trial_num = 5

    for N in range(0, trial_num):
        xi = random.uniform(-1, 1)
        phi_i = np.array([[1.0, xi]])
        ti = t(a0, a1, xi)

        if N == 0:
            print 'M ', M
            print 'S ', S
            xn = np.array([[xi]])
            tn = np.array([[xi]])
            phi = phi_i
            ax = fig.add_subplot(trial_num + 1, 3, 2)
            draw_probability(ax, M, S)

        else:
            xn = np.r_[xn, np.array([[xi]])]
            tn = np.r_[tn, np.array([[ti]])]
            phi = np.r_[phi, phi_i]

        phi_T = phi.T
        S_inv = alpha * I + beta * phi_T.dot(phi)
        S = la.inv(S_inv)
        S_phi_T = S.dot(phi_T)
        S_phi_T_t = S_phi_T.dot(tn)
        M = beta * S_phi_T_t
        print 'M ', M
        print 'S ', S

        ax = fig.add_subplot(trial_num + 1, 3, 3 * N + 4)
        draw_likelihood(beta, ti, xi, ax=ax)
        ax = fig.add_subplot(trial_num + 1, 3, 3 * N + 5)
        draw_probability(ax, M, S)

    plt.show()
    exit(0)
