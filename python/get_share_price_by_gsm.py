#!/usr/bin/env python
# -*- coding: utf-8 -*-

import jsm
import datetime
import pandas as pd
import matplotlib.pyplot as plt

q = jsm.Quotes()
start_date = datetime.date(2015, 1, 1)
end_date = datetime.date(2015, 2, 13)

nomura_holdings = q.get_historical_prices(8604, jsm.DAILY,
                                          start_date=start_date,
                                          end_date=end_date)

# daiwa_securities = q.get_historical_prices(8601, jsm.DAILY,
#                                            start_date=start_date,
#                                            end_date=end_date)

nomura_stock_list = [each_day_data.date for each_day_data in nomura_holdings]
nomura_date_list = [each_day_data.date for each_day_data in nomura_holdings]
nomura_open_list = [each_day_data.open for each_day_data in nomura_holdings]
nomura_close_list = [each_day_data.close for each_day_data in nomura_holdings]
nomura_high_list = [each_day_data.high for each_day_data in nomura_holdings]
nomura_low_list = [each_day_data.low for each_day_data in nomura_holdings]

for (date, open, close, high, low) in zip(nomura_date_list[0:10],
                                          nomura_open_list[0:10],
                                          nomura_close_list[0:10],
                                          nomura_high_list[0:10],
                                          nomura_high_list[0:10]):
    print(date, open, close, high, low)

nomura_date_str = [str(x)[0:10] for x in nomura_date_list]
nomura_daily_stock = {'date': nomura_date_str,
                      'open': nomura_open_list,
                      'close': nomura_close_list,
                      'high': nomura_high_list,
                      'low': nomura_low_list}
nomura_daily_stock_df = pd.DataFrame(nomura_daily_stock)

fig = plt.figure()


exit(0)
