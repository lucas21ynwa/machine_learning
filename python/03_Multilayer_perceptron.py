#coding:utf-8
import math
import numpy as np
import matplotlib as plt
import pylab
import random

def sig(x):
	return 1/(1+np.exp(-x))
def dsig(x):
	return sig(x)*(1-sig(x))
def calcHiddenLayer(data, wInHidden):
	tmp = np.zeros(np.size(data)+1)
	tmp[0] = 1
	tmp[1:] = data
	ret = np.zeros(np.size(wInHidden[0,:])+1)
	ret[0] = 1
	ret[1:] = np.transpose(tmp.dot(wInHidden))
	return sig(ret)
def calcHiddenLayerDsig(data, wInHidden):
	tmp = np.zeros(np.size(data)+1)
	tmp[0] = 1
	tmp[1:] = data
	ret = np.zeros(np.size(wInHidden[0,:])+1)
	ret[0] = 1
	ret[1:] = np.transpose(tmp.dot(wInHidden))
	return dsig(ret)
def calcOut(hiddenLayer, wHiddenOut):
	tmp = hiddenLayer
	ret = np.zeros(np.size(wHiddenOut[0,:]))
	ret[:] = np.transpose(tmp.dot(wHiddenOut))
	return ret
def calcErrorHidden(out, wHiddenOut):
	tmp = wHiddenOut
	ret = tmp.dot(out)
	return ret

if __name__ == "__main__":
	#--Loading training data--#
	train   = np.loadtxt("polyData.txt", delimiter="\t")
	train_x = train[:, 0]
	train_y = train[:, 1]

	#--Definition of internal parameters--#
	dataDim = 1
	numActivation = 4
	outDim  = 1
	data = np.zeros(dataDim)
	inin         = np.zeros(dataDim + 1)
	wInHidden    = np.ones((dataDim + 1, numActivation))
	dwInHidden   = np.zeros((dataDim + 1, numActivation))
	hiddenLayer  = np.zeros(numActivation + 1)
	wHiddenOut   = np.ones((numActivation + 1, outDim))
	dwHiddenOut  = np.zeros((numActivation + 1, outDim))
	out          = np.zeros(outDim)
	errorOut     = np.zeros(outDim)
	errorHidden  = np.zeros(numActivation + 1)
	learningRate = 0.1
	iteration    = 100000
	
	#--Initialization of w--#
	for i in range(np.size(wInHidden[:, 0])):
		for j in range(np.size(wInHidden[0, :])):
			wInHidden[i, j] = random.uniform(-0.1, 0.1)
	for i in range(np.size(wHiddenOut[:,0])):
		for j in range(np.size(wHiddenOut[0, :])):
			wHiddenOut[i, j] = random.uniform(-0.1, 0.1)

	#--Calculation of back propagation--#
	for iter in range(iteration):
		index       = random.randint(0, np.size(train_x)-1)
		data[:]     = train_x[index]
		inin[0]     = 1
		inin[1:]    = data
		hiddenLayer = calcHiddenLayer(data, wInHidden) 
		out         = calcOut(hiddenLayer, wHiddenOut)
		errorOut    = out-train_y[index]
		errorHidden = calcHiddenLayerDsig(data, wInHidden)*calcErrorHidden(errorOut, wHiddenOut) #hiddenLayer*(1-hiddenLayer)*calcErrorHidden(errorOut, wHiddenOut)
		for i in range(np.size(inin)):
			dwInHidden[i, :]   = errorHidden[1:]*inin[i]
		for i in range(np.size(out)):
			dwHiddenOut[:, i]  = hiddenLayer*errorOut[i]
		wInHidden   = wInHidden  - learningRate * dwInHidden
		wHiddenOut  = wHiddenOut - learningRate * dwHiddenOut
		print errorOut

	#--Plot the data--#
	fig1 = pylab.figure()
	pylab.subplot(1,1,1)
	pylab.plot(train_x, train_y, 'bo')
	fit_x = train_x
	fit_y = np.zeros(np.size(fit_x))
	for t in range(np.size(fit_y)):
		hiddenLayer = calcHiddenLayer(fit_x[t], wInHidden)
		fit_y[t] = calcOut(hiddenLayer, wHiddenOut)
	
	pylab.subplot(1,1,1)
	pylab.plot(fit_x, fit_y, 'r-')

	pylab.show()
	pylab.close()
