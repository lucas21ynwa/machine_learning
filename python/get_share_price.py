#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import pandas as pd
import matplotlib.pyplot as plt
import get_quote_by_pandas as quote
import xlsxwriter


start_date = datetime.date(2015, 1, 1)
end_date = datetime.date(2015, 2, 13)
nomura_stock = quote.get_quote_yahoojp(8604, start=start_date, end=end_date)
nomura_stock = nomura_stock[-30:]


# nomura_stock.to_excel(u'野村Hldg.株価日次推移（原型列）計数データ.xls', sheet_name=u'計数データ')

nomura_stock.plot(kind='ohlc')

nomura_stock_high_5days_roll_mean = pd.rolling_mean(nomura_stock['High'], 5)
nomura_stock_high_5days_roll_mean.plot()
nomura_stock_high_20days_roll_mean = pd.rolling_mean(nomura_stock['High'], 20)
nomura_stock_high_20days_roll_mean.plot()
nomura_stock_high_75days_roll_mean = pd.rolling_mean(nomura_stock['High'], 75)
nomura_stock_high_75days_roll_mean.plot()

plt.show()

exit(0)
