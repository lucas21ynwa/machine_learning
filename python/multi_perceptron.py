#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import numpy as np
import random
import matplotlib.pyplot as plt
# import myplot

PI = np.pi
MAX_ITER = 100000
IN_NUM = 1
HID_NUM = 4
OUT_NUM = 1
SAMPLE_NUM = 101

ETA = 0.1


# sigmoid function
def sigmoid(_x):
    return 1.0 / (1.0 + np.exp(-_x))


# differential of sigmoid function
def d_sigmoid(_x):
    sig_x = sigmoid(_x)
    return sig_x * (1 - sig_x)


def tanh(_x):
    return np.tanh(_x)


def d_tanh(_x):
    tanh_x = np.tanh(_x)
    return 1 - tanh_x ** 2


def init_weight(_w):
    row = _w.shape[0]
    col = _w.shape[1]

    for i in range(0, row):
        for j in range(0, col):
            _w[i, j] = random.uniform(-0.1, 0.1)


# make test data of sin function
def make_input_data(_data_path=False):
    x_data = np.linspace(-PI, PI, SAMPLE_NUM)
    # t_data = np.sin(x_data[:])
    t_data = np.cos(x_data[:])

    if _data_path is not False:
        with open(_data_path) as fout:
            fout.wrtie(_data_path)
            print _data_path, 'wrote'

    return x_data, t_data


def forward_prop(input_i, W1, W2):
    IN = np.zeros((1, IN_NUM + 1))
    IN[0, 0] = 1.0  # bias parameter
    IN[0, 1] = input_i
    IN_T = IN.transpose()

    HID_T = W1.dot(IN_T)
    HID_T = sigmoid(HID_T[:])
    # HID_T = np.tanh(HID_T[:])
    HID_T[0, 0] = 1.0
    HID = HID_T.transpose()

    OUT = W2.dot(HID_T)

    return IN, HID, OUT


def back_prop(true_output, IN, HID, OUT, W1, W2):
    # dIN = d_sigmoid(IN[:])
    # dIN_T = dIN.transpose()

    TRUE = np.zeros((1, OUT_NUM))
    TRUE[0, 0] = true_output
    eOUT = OUT - TRUE
    eOUT_T = eOUT.transpose()
    print eOUT[0]

    W2_T = W2.transpose()
    tmp = W2_T.dot(eOUT_T)
    tmp = tmp.transpose()
    eHID = np.zeros([1, HID_NUM + 1])
    eHID[:] = (1 - HID[:]) * HID[:] * tmp[:]
    eHID_T = eHID.transpose()
    # eHID_T = dIN_T * TMP
    # eHID = eHID_T.transpose()
    # eHID_T = d_sigmoid(eHID_T[:])

    d_W2 = eOUT_T.dot(HID)
    d_W1 = eHID_T.dot(IN)

    W2 = W2 - ETA * d_W2
    W1 = W1 - ETA * d_W1

    return W1, W2


def plot_func(_x_data, _y_data, _t_data):

    fig = plt.figure()
    fig.patch.set_alpha(0.0)
    ax = fig.add_subplot(1, 1, 1)
    plt.tick_params(axis='x', which='both', top='off')
    plt.tick_params(axis='y', which='both', right='off')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.plot(_x_data, _t_data, 'o', color='black', label='train data')
    ax.plot(_x_data, _y_data, '-', color='red', label='fittign_data')
    ax.set_xlim(-PI, PI)
    ax.set_ylim(-1.2, 1.2)

    plt.show()

# main
if __name__ == '__main__':
    argv = sys.argv
    argc = len(argv)

    if argc == 2:
        input_path = argv[1]

        if os.path.isfile(input_path) is False:
            print input_path
            print 'does not exist'
            exit(-1)

        data_dir, input_file = os.path.split(input_path)

    else:
        data_dir = '.'

    # make sin data
    x_data, t_data = make_input_data()

    # input_layer  IN
    # IN = np.zeros((1, IN_NUM + 1))
    # hidden_layer HID
    # HID = np.zeros((1, HID_NUM + 1))
    # output_layer OUT
    # OUT = np.zeros((1, OUT_NUM))
    # TRUE_OUT = np.zeros((1, OUT_NUM))

    # w matrix from input to hidden
    W1 = np.ones([HID_NUM + 1, IN_NUM + 1])
    # w matrix from hidden to output
    W2 = np.ones([OUT_NUM, HID_NUM + 1])

    init_weight(W1)
    init_weight(W2)

    for i in range(0, MAX_ITER):
        train_index = random.randint(0, SAMPLE_NUM - 1)
        in_i = x_data[train_index]
        out_i = t_data[train_index]

        IN, HID, OUT = forward_prop(in_i, W1, W2)
        W1, W2 = back_prop(out_i, IN, HID, OUT, W1, W2)

    y_data = np.zeros(SAMPLE_NUM)

    for i in range(0, SAMPLE_NUM):
        in_i = x_data[i]
        IN, HID, OUT = forward_prop(in_i, W1, W2)
        y_data[i] = OUT[0, 0]

    plot_func(x_data, y_data, t_data)

    exit(0)
